<?php

include 'include/include_all.php';

class gordao_file
{
  function __construct($filename) {
    $lines = file($filename);

    foreach($lines as $key => $value)
    {
      $single_line = $lines[$key];
      $split_line = explode('|',$single_line);

      $time = trim($split_line[0]);
      $type = trim($split_line[1]);

      if($type == "start_ride_time") $this->start_ride_time = $time;
      if($type == "end_ride_time") $this->end_ride_time = $time;

      if(!isset($datapoints)) $datapoints = 0;

      $this->datapoints = $datapoints++;
    }

    $this->duration = ($this->end_ride_time - $this->start_ride_time)/1000;

    foreach($lines as $key => $value)
    {
      $single_line = $lines[$key];

      $split_line = explode('|',$single_line);

      $time = trim($split_line[0]);
      $type = trim($split_line[1]);

      if($type == "start_ride_time") $this->start_ride_time = $time;
      if($type == "end_ride_time") $this->end_ride_time = $time;
      if($type == "location")
      {
        $this->coords_times[] = $time;
        $this->coords[$time] = new stdClass();
        $this->coords[$time]->latitude = $split_line[2];
        $this->coords[$time]->longitude =   str_replace(array("\n", "\r"), '', trim($split_line[3]));
      }
    }
  }

  function gpx_build()
  {
    $gpx_output = '
    <?xml version="1.0" encoding="UTF-8"?>
    <gpx creator="StravaGPX" version="1.1" xmlns="http://www.topografix.com/GPX/1/1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd http://www.garmin.com/xmlschemas/GpxExtensions/v3 http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd http://www.garmin.com/xmlschemas/TrackPointExtension/v1 http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd http://www.garmin.com/xmlschemas/GpxExtensions/v3 http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd http://www.garmin.com/xmlschemas/TrackPointExtension/v1 http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd http://www.garmin.com/xmlschemas/GpxExtensions/v3 http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd http://www.garmin.com/xmlschemas/TrackPointExtension/v1 http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd http://www.garmin.com/xmlschemas/GpxExtensions/v3 http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd http://www.garmin.com/xmlschemas/TrackPointExtension/v1 http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd http://www.garmin.com/xmlschemas/GpxExtensions/v3 http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd http://www.garmin.com/xmlschemas/TrackPointExtension/v1 http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd" xmlns:gpxtpx="http://www.garmin.com/xmlschemas/TrackPointExtension/v1" xmlns:gpxx="http://www.garmin.com/xmlschemas/GpxExtensions/v3">
    <metadata>
    <time>'.str_replace('+00:00', 'Z', gmdate('c', round($this->start_ride_time/1000))).'</time>
    </metadata>
    <trk>
    <name>GordaoCPU made this</name>
    <trkseg>
    ';

    foreach ($this->coords as $key => $value)
    {
      $gpx_output .= '  <trkpt lat="'.$this->coords[$key]->latitude.'" lon="'.$this->coords[$key]->longitude.'">
';
      $gpx_output .= '      <time>'.str_replace('+00:00', 'Z', gmdate('c', round($key/1000))).'</time>
      ';
      // if($_integr[$key][heartrate] != "")  $gpx_output .= '<extensions>\n<gpxtpx:TrackPointExtension>\n<gpxtpx:hr>'.$_integr[$key][heartrate].'</gpxtpx:hr></gpxtpx:TrackPointExtension></extensions>';
      $gpx_output .= '</trkpt>
      ';
    }
    $gpx_output .= '
    </trkseg>
  </trk>
</gpx>';
    return $gpx_output;
  }

  function encoded_coords()
  {
    foreach ($this->coords as $key => $value)
    {
      $points[] = array($this->coords[$key]->latitude,$this->coords[$key]->longitude);
    }
    $encoded = Polyline::encode($points);
    return $encoded;
  }

  function path_map_url($size = "350x300")
  {
    global $Google_APIs_Key;
    $url = 'https://maps.googleapis.com/maps/api/staticmap?size='.$size.'&0&path=weight:3|color:red|enc:'.$this->encoded_coords().'&key='.$Google_APIs_Key;
    return $url;
  }
}

?>
